﻿using System;
using System.Runtime.Serialization;

namespace CodingChallenge8
{
    [Serializable]
    internal class NameUniqueException : Exception
    {
        public NameUniqueException()
        {
        }

        public NameUniqueException(string message) : base(message)
        {
        }

        
    }
}